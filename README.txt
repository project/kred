README
======
The Kred module allows you to integrate your drupal site with the kred api
system (http://www.kred.com)

In real life social circles, people are influenced by their peers. On social
networks, influencers are trusted friends and authorities within affinity
based communities.

Kred, created by PeopleBrowsr, measures influence in online communities
connected by interest. Kred values audience quality and engagement over
audience size. It assesses the ability to inspire action (Influence) and
propensity to engage with others (Outreach).

Installation:
- Extrat Kred folder to your modules folder
- Enable module
- configure your Kred account (go to 'admin/config/services/kred')
  Please look at the following links in order to have the correct API
  configuration:
    * https://developer.peoplebrowsr.com/kred/kredscore
    * https://developer.peoplebrowsr.com/kred/kredcommunity
    * https://developer.peoplebrowsr.com/kred/kredentials
